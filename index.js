var app = require('express')();
var bodyParser = require('body-parser');
var apiRouter = require('./api-router');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});

apiRouter(app);

app.listen(3000, function(){
	console.log('shopping cart app started at port 3000');
});