var uuid = require('uuid');
var _ = require('lodash');
var items = [];

function getItems() {
	return items;
}

function getItemById(itemId) {
	return _.find(items, {id: itemId});
}

function addItems(newItems){
	if(newItems && newItems.length > 0){
		_.each(newItems, function(newItem){
			newItem.id = uuid.v4();
		});
		items.push(...newItems);
		return true;	
	} else {
		return false;
	}
}

function deleteItem(itemId){
	_.remove(items, {id: itemId});
}

function updateItem(itemId, newItem){
	var item = _.find(items, {id: itemId});
	if(item){
		Object.assign(item, newItem);
		items.splice(_.indexOf(items, item), 1, item);
		return true;
	} else {
		return false;
	}
}

exports = module.exports = {
	getItemById: getItemById,
	getItems: getItems,
	addItems: addItems,
	deleteItem: deleteItem,
	updateItem: updateItem
};