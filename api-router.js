var apiRouter = require('express').Router();
var db = require('./database');

apiRouter.route('/items')
	.get(function(req, res){
		var items = db.getItems();
		res.status(200).json(items);
	})
	.post(function(req, res){
		var result = db.addItems(req.body.items);
		if(result){
			res.status(201).json({ok: true});
		} else {
			res.status(200).json({ok: false, message: 'No Items to be added to cart'});
		}
	})
	.delete(function(req, res){
		var itemId = req.query.itemId;
		if(itemId){
			db.deleteItem(itemId);
			res.status(200).json({ok: true});
		} else {
			res.status(200).json({ok: false, message: 'Item not found'});
		}
		
	})
	.put(function(req, res){
		var itemId = req.query.itemId;
		if(itemId){
			db.updateItem(itemId, {
				name: req.body.name,
				quantity: req.body.quantity
			});
			res.status(200).json({ok: true});
		} else {
			res.status(200).json({ok: false, message: 'Item not found'});
		}
	});

	apiRouter.get('/items/:itemId', function(req, res){
		var itemId = req.params.itemId;
		var item = db.getItemById(itemId);
		
		if(item){
			res.status(200).json(item);
		} else {
			res.status(200).json({ok: false, message: 'Item not found'});
		}
		
	});
	
	
exports = module.exports = function(app){
	app.use('/api', apiRouter);
}